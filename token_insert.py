def main(mess, c, connection, vk, m, bot, tel):
    import requests
    import random
    import time
    import os
    import send
    from vk_api.keyboard import VkKeyboard, VkKeyboardColor
    keyboard = VkKeyboard(one_time=True)
    keyboard.add_button('Токен', color=VkKeyboardColor.PRIMARY)
    keyboard.add_line()
    keyboard.add_button('Меню', color=VkKeyboardColor.PRIMARY)
    login_id = os.environ.get('autopost_id')
    try:
        token = mess.split("access_token=")[1].split("&")[0]
    except:
        send.messages.send(message = "Неверная ссылка с токеном", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], tel=tel, bot=bot)
        send.messages.send(message = "Инструкция по получению токена:\nhttp://vk.com/@ilyanik0103-autopostauth", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), tel=tel, bot=bot)
        return "failed"
    test_delete = requests.get("https://api.vk.com/method/messages.getHistory?peer_id=-{}&count=5&access_token={}&v=5.101".format(login_id, token)).json()
    try:
        if tel != 1:
            requests.get("https://api.vk.com/method/messages.delete?message_ids={}&delete_for_all=1&access_token={}&v=5.101".format(test_delete["response"]["items"][0]["id"], token)).json()
            vk.messages.delete(message_ids=m["id"])
    except:
        pass
    try:
        user_id = mess.split("user_id=")[1]
    except:
        send.messages.send(message = "Неверная ссылка с токеном, нет ID пользователя", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], tel=tel, bot=bot)
        send.messages.send(message = "Инструкция по получению токена:\nhttp://vk.com/@ilyanik0103-autopostauth", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), tel=tel, bot=bot)
        return "failed"
    try:
        if int(user_id) != int(m["peer_id"]) and tel != 1:
            send.messages.send(message = "Этот токен профиля vk.com/id{}.\nСкорее всего у вас в браузере введен другой профиль\nПопробуйте войти в браузере в свой профиль и открыть ссылку еще раз:\nhttps://oauth.vk.com/authorize?client_id=2685278&display=page&scope=offline,messages&redirect_uri=https://api.vk.com/blank.html&response_type=token&v=5.88".format(user_id), random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], tel=tel, bot=bot)
            send.messages.send(message = "Инструкция по получению токена:\nhttp://vk.com/@ilyanik0103-autopostauth", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), tel=tel, bot=bot)
            return "failed"
    except:
        send.messages.send(message = "Неверная ссылка с токеном, ID пользователя не численный", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], tel=tel, bot=bot)
        send.messages.send(message = "Инструкция по получению токена:\nhttp://vk.com/@ilyanik0103-autopostauth", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), tel=tel, bot=bot)
        return "failed"

    send.messages.send(message = "Проверка токена", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], tel=tel, bot=bot)
    if tel == 1:
        test_result = requests.get("https://api.vk.com/method/messages.getConversations?access_token={}&v=5.124".format(token)).json()
    else:
        test_result = requests.get("https://api.vk.com/method/messages.send?peer_id=-{}&message=Проверка успешна&random_id={}&access_token={}&v=5.90".format(login_id, random.randint(1000000000, 1000000000000), token)).json()
    if "error" in test_result:
        code = int(test_result["error"]["error_code"])
        if code == 902:
            send.messages.send(message = "Ошибка добавления токена (902), невозможно написать в этот чат для проверки токена, попробуйте еще раз через браузер", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), tel=tel, bot=bot)
        elif code == 7:
            send.messages.send(message = "Ошибка добавления токена (7), недостаточно разрешений чтобы написать в этот чат для проверки токена, попробуйте еще раз через браузер", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), tel=tel, bot=bot)
        elif code == 6 or code == 9:
            send.messages.send(message = "Ошибка добавления токена (6/9), слишком много запросов в секунду, попробуйте еще раз", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), tel=tel, bot=bot)
        elif code == 10:
            send.messages.send(message = "Ошибка добавления токена (10), внутренняя ошибка Вконтакте, попробуйте еще раз", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), tel=tel, bot=bot)
        elif code == 14:
            send.messages.send(message = "Ошибка добавления токена (14), невозможно выполнить проверку из-за капчи, попробуйте еще раз", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), tel=tel, bot=bot)
        elif code == 5:
            send.messages.send(message = "Ошибка добавления токена (5), токен не верный, убедитесь что вы полностью скопировали ссылку", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), tel=tel, bot=bot)
        else:
            send.messages.send(message = "Неизвестная ошибка добавления токена, попробуйте получить токен еще раз", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), tel=tel, bot=bot)
        return "failed"
    time.sleep(1)
    c.execute("select exists(select * from users where vkid = {})".format(user_id))
    if c.fetchone()[0] == 1:
        if tel == 1:
            c.execute("select vkid from users where tid = {}".format(m["from"]["id"]))
            old_vk_id = c.fetchone()
            #c.execute("update users set tid = 0, id = vkid where tid = {}".format(m["from"]["id"]))
            if old_vk_id != None:
                old_vk_id = old_vk_id[0]
                c.execute("update jobs set id = {} where id = {}".format(old_vk_id, m["from"]["id"]))
            c.execute("update users set id = vkid where id = {}".format(m["from"]["id"]))
            connection.commit()


            c.execute("update users set tid = {}, id = {} where vkid = {}".format(m["from"]["id"], m["from"]["id"], user_id))
        c.execute("update users set token = '{}', fails = 0 where vkid = {}".format(token, user_id))
        send.messages.send(message = "Токен обновлён", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], tel=tel, bot=bot)
    else:
        if tel == 1:
            tel_id = m["peer_id"]
            sender_id = m["from"]["id"]
            c.execute("update users set tid = 0, id = vkid where tid = {}".format(m["from"]["id"]))
        else:
            tel_id = 0
            sender_id = m["peer_id"]
        c.execute("insert into users values ({}, '{}', 'menu', 0, 0, {}, {})".format(sender_id, token, tel_id, user_id))
        send.messages.send(message = "Вы можете создавать задания", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], tel=tel, bot=bot)
    #c.execute("select exists(select * from ids where tel = {})".format(m["peer_id"]))
    #telidex = c.fetchone()[0]
    if tel == 1:
        c.execute("update jobs set user_id = {} where user_id = {}".format(m["from"]["id"], user_id))
    else:
        try:
            c.execute("select tid from users where vkid = {}".format(m["peer_id"]))
            c.execute("update jobs set user_id = {} where user_id = {}".format(m["peer_id"], c.fetchone()[0]))
        except:
            pass
    import redis
    import config
    r = redis.Redis(host=config.redis_ip, port=6379)
    r.delete("autopost.users.{}".format(m["peer_id"]))
    connection.commit()
    
    import menu
    menu.main(mess, c, connection, vk, m, bot, tel)

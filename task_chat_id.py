def main(mess, c, connection, vk, m, bot, tel):
    from vk_api.keyboard import VkKeyboard, VkKeyboardColor
    keyboard = VkKeyboard(one_time=True)
    keyboard.add_button('Отмена', color=VkKeyboardColor.PRIMARY)
    import random
    import send
    if mess.isdigit() != True:
        send.messages.send(message = "Неверный номер чата\nВведите номер чата из списка (только цифры)", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
    chat_id = int(mess) + 2000000000
    c.execute("select max(id) from jobs")
    max_id = c.fetchone()[0]
    if max_id == None:
        max_id = 1
    c.execute("insert into jobs values ({}, {}, 0, 0, 0, 0, {}, 0, 0, 0, 1, 0)".format(m["peer_id"], chat_id, max_id + 1))
    connection.commit()
    c.execute("update users set status = 'task_text' where id = {}".format(m["peer_id"]))
    connection.commit()
    keyboard = VkKeyboard(one_time=True)
    import create_premium_abort
    if create_premium_abort.check(mess, c, connection, vk, m, bot, tel, position="select_button") == "failed":
        keyboard.add_button('Выбор кнопки', color=VkKeyboardColor.NEGATIVE)
    else:
        keyboard.add_button('Выбор кнопки', color=VkKeyboardColor.PRIMARY)
    keyboard.add_line()
    keyboard.add_button('Отмена', color=VkKeyboardColor.PRIMARY)
    send.messages.send(message = "Введите сообщение, которое будет отправляться\nЕсли нужно нажимать на кнопку, то нажмите Выбор кнопки", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)

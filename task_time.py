def add_time_keyboard(mess, c, connection, vk, m, keyboard, bot, tel, israndom):
    from vk_api.keyboard import VkKeyboard, VkKeyboardColor
    import create_premium_abort
    if create_premium_abort.check(mess, c, connection, vk, m, bot, tel, position="time_select") == "failed":

        keyboard.add_button('30 секунд', color=VkKeyboardColor.NEGATIVE)
        keyboard.add_button('40 секунд', color=VkKeyboardColor.NEGATIVE)
        keyboard.add_button('50 секунд', color=VkKeyboardColor.NEGATIVE)
        keyboard.add_line()
        keyboard.add_button('1 минута', color=VkKeyboardColor.NEGATIVE)
        keyboard.add_button('5 минут', color=VkKeyboardColor.PRIMARY)
        keyboard.add_button('15 минут', color=VkKeyboardColor.PRIMARY)
        keyboard.add_button('30 минут', color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
    else:
        if israndom == 0:
            keyboard.add_button('30 секунд', color=VkKeyboardColor.PRIMARY)
            keyboard.add_button('40 секунд', color=VkKeyboardColor.PRIMARY)
            keyboard.add_button('50 секунд', color=VkKeyboardColor.PRIMARY)
            keyboard.add_button('1 минута', color=VkKeyboardColor.PRIMARY)
            keyboard.add_line()
            keyboard.add_button('5 минут', color=VkKeyboardColor.PRIMARY)
            keyboard.add_button('10 минут', color=VkKeyboardColor.PRIMARY)
            keyboard.add_button('30 минут', color=VkKeyboardColor.PRIMARY)
            keyboard.add_line()
        else:
            keyboard.add_button('1 минута', color=VkKeyboardColor.PRIMARY)
            keyboard.add_button('5 минут', color=VkKeyboardColor.PRIMARY)
            keyboard.add_button('10 минут', color=VkKeyboardColor.PRIMARY)
            keyboard.add_button('30 минут', color=VkKeyboardColor.PRIMARY)
            keyboard.add_line()

    keyboard.add_button('1 час', color=VkKeyboardColor.PRIMARY)
    keyboard.add_button('2 часа', color=VkKeyboardColor.PRIMARY)
    keyboard.add_button('5 часов', color=VkKeyboardColor.PRIMARY)
    if israndom == 0:
        keyboard.add_line()
        if create_premium_abort.check(mess, c, connection, vk, m, bot, tel, position="time_select") == "failed":
            keyboard.add_button('Случайное время', color=VkKeyboardColor.NEGATIVE)
        else:
            keyboard.add_button('Случайное время', color=VkKeyboardColor.PRIMARY)
    keyboard.add_line()
    keyboard.add_button('Отмена', color=VkKeyboardColor.PRIMARY)
    return keyboard
    
def main(mess, c, connection, vk, m, bot, tel, israndom):
    import random
    import send
    from vk_api.keyboard import VkKeyboard, VkKeyboardColor
    keyboard = VkKeyboard(one_time=True)

    try:
        if mess[-1] == "с" and mess[-2].isdigit() == True:
            delay = int(mess[:-1])
        elif mess[-1] == "м":
            delay = int(mess[:-1])*60
        elif mess[-1] == "ч":
            delay = int(mess[:-1])*3600
        elif mess[-1] == "д" and mess[-2].isdigit() == True:
            delay = int(mess[:-1])*86400
        elif mess == "час":
            delay = 3600
        elif mess == "минута":
            delay = 60
        elif mess.count("час") == 1 or mess.count("часа") == 1 or mess.count("часов") == 1:
            delay = int(mess.split(" ")[0])*3600
        elif mess.count("минута") == 1 or mess.count("минуты") == 1 or mess.count("минут") == 1:
            delay = int(mess.split(" ")[0])*60
        elif mess.count("секунда") == 1 or mess.count("секунды") == 1 or mess.count("секунд") == 1:
            delay = int(mess.split(" ")[0])
        elif mess.isdigit() == True:
            delay = int(mess)
        else:
            keyboard = add_time_keyboard(mess, c, connection, vk, m, keyboard, bot, tel, israndom)
            send.messages.send(message = "Частота выполнения задания указана неправильно\nВведите частоту выполнения в секундах", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
            return "failed"
    except:
        keyboard = add_time_keyboard(mess, c, connection, vk, m, keyboard, bot, tel, israndom)
        send.messages.send(message="Частота выполнения задания указана неправильно\nВведите частоту выполнения в секундах", random_id=random.randint(1000000000, 1000000000000), peer_id=m["peer_id"], keyboard=keyboard.get_keyboard(), bot=bot, tel=tel)
        return "failed"
    
    if delay < 30:
        keyboard = add_time_keyboard(mess, c, connection, vk, m, keyboard, bot, tel, israndom)
        send.messages.send(message = "Частота отправки сообщений не может быть меньше 30 секунд\nВведите другое время", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
        return "passed"
    if delay <= 299:
        import create_premium_abort
        if create_premium_abort.check(mess, c, connection, vk, m, bot, tel, position = "time_create") == "failed":
            return "failed"
    
    import menu
    if delay <= 60:
        c.execute("select count(*) from jobs where user_id = {} and delay <= 60 and delay != 0 and enabled = 1".format(m["peer_id"]))
        delay_count = c.fetchall()[0][0]
        if delay_count != None and delay_count > 0:
            keyboard = add_time_keyboard(mess, c, connection, vk, m, keyboard, bot, tel, israndom)
            send.messages.send(peer_id=m["peer_id"], message="Заданий с частотой отправки 60 сек и менее не может быть более одного во избежание капчи\nВведите другое время", random_id = random.randint(1000000000, 1000000000000), keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
            return "failed"

    elif delay <= 120:
        c.execute("select count(*) from jobs where user_id = {} and delay <= 120 and delay != 0 and enabled = 1".format(m["peer_id"]))
        delay_count = c.fetchall()[0][0]
        if delay_count != None and delay_count > 1:
            keyboard = add_time_keyboard(mess, c, connection, vk, m, keyboard, bot, tel, israndom)
            send.messages.send(peer_id=m["peer_id"], message="Заданий с частотой отправки 2 минуты и менее не может быть более двух во избежание капчи\nВведите другое время", random_id = random.randint(1000000000, 1000000000000), keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
            return "failed"

    elif delay <= 300:
        c.execute("select count(*) from jobs where user_id = {} and delay <= 300 and delay != 0 and enabled = 1".format(m["peer_id"]))
        delay_count = c.fetchall()[0][0]
        if delay_count != None and delay_count > 4:
            keyboard = add_time_keyboard(mess, c, connection, vk, m, keyboard, bot, tel, israndom)
            send.messages.send(peer_id=m["peer_id"], message="Заданий с частотой отправки 5 минут и менее не может быть более пяти во избежание капчи\nВведите другое время", random_id = random.randint(1000000000, 1000000000000), keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
            return "failed"

    elif delay <= 600:
        c.execute("select count(*) from jobs where user_id = {} and delay <= 600 and delay != 0 and enabled = 1".format(m["peer_id"]))
        delay_count = c.fetchall()[0][0]
        if delay_count != None and delay_count > 9:
            keyboard = add_time_keyboard(mess, c, connection, vk, m, keyboard, bot, tel, israndom)
            send.messages.send(peer_id=m["peer_id"], message="Заданий с частотой отправки 10 минут и менее не может быть более десяти во избежание капчи\nВведите другое время", random_id = random.randint(1000000000, 1000000000000), keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
            return "failed"

    elif delay <= 1800:
        c.execute("select count(*) from jobs where user_id = {} and delay <= 1800 and delay != 0 and enabled = 1".format(m["peer_id"]))
        delay_count = c.fetchall()[0][0]
        if delay_count != None and delay_count > 29:
            keyboard = add_time_keyboard(mess, c, connection, vk, m, keyboard, bot, tel, israndom)
            send.messages.send(peer_id=m["peer_id"], message="Заданий с частотой отправки 30 минут и менее не может быть более 30-ти во избежание капчи\nВведите другое время", random_id = random.randint(1000000000, 1000000000000), keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
            return "failed"

    elif delay <= 3600:
        c.execute("select count(*) from jobs where user_id = {} and delay <= 3600 and delay != 0 and enabled = 1".format(m["peer_id"]))
        delay_count = c.fetchall()[0][0]
        if delay_count != None and delay_count > 59:
            keyboard = add_time_keyboard(mess, c, connection, vk, m, keyboard, bot, tel, israndom)
            send.messages.send(peer_id=m["peer_id"], message="Заданий с частотой отправки 60 минут и менее не может быть более 60-ти во избежание капчи\nВведите другое время", random_id = random.randint(1000000000, 1000000000000), keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
            return "failed"

    
    
    c.execute("select max(id) from jobs where user_id = {}".format(m["peer_id"]))
    task_id = c.fetchone()[0]
    c.execute("select * from jobs where id = {}".format(task_id))
    task_info = c.fetchone()
    c.execute("select token from users where id = {}".format(m["peer_id"]))
    user_token = c.fetchone()[0]
    
    import requests
    if str(task_info[1])[0] == "-":
        #group_info = vk.groups.getById(group_ids = str(task_info[1])[1:])
        group_info = requests.get("https://api.vk.com/method/groups.getById?v=5.111&access_token=8babc42c8babc42dd2e002&group_ids={}".format(str(task_info[1])[1:])).json()["response"]
        chat_name = group_info[0]["name"]
        name_text = "Название группы"
    elif int(task_info[1]) < 2000000000:
        #user_info = vk.users.get(user_ids = task_info[1])
        user_info = requests.get("https://api.vk.com/method/users.get?v=5.111&access_token=8babc42c8babc42c8babc42cd98e002&user_ids={}".format(task_info[1])).json()["response"]
        chat_name = "{} {}".format(user_info[0]["first_name"], user_info[0]["last_name"])
        name_text = "Имя и фамилия в профиле"
    elif int(task_info[1]) > 2000000000:
        c.execute("select token from users where id = {}".format(m["peer_id"]))
        user_token = c.fetchone()
        if user_token == None:
            return "failed"
        user_token = user_token[0]
        
        import requests
        get_conversations = requests.get("https://api.vk.com/method/messages.getConversationsById?&access_token={}&peer_ids={}&v=5.88".format(user_token, task_info[1])).json()
        if "response" in get_conversations:
            chat_name = get_conversations["response"]["items"][0]["chat_settings"]["title"]
        else:
            return "failed"
        name_text = "Название чата"
    
        
    import requests
    test_result = requests.get("https://api.vk.com/method/messages.send?peer_id={}&message={}&random_id={}&payload={}&access_token={}&v=5.90".format(
        task_info[1], task_info[5], random.randint(1000000000, 1000000000000), task_info[11], user_token)).json()
    if "error" in test_result:
        keyboard.add_button('Назад в меню', color=VkKeyboardColor.PRIMARY)
        code = int(test_result["error"]["error_code"])
        import task_abort
        if code == 902:
            task_abort.main(mess, c, connection, vk, m, bot, tel, spy = 1)
            send.messages.send(message = "Ошибка создания задания (902), невозможно написать в этот чат", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
        elif code == 7:
            task_abort.main(mess, c, connection, vk, m, bot, tel, spy = 1)
            send.messages.send(message = "Ошибка создания задания (7), недостаточно разрешений чтобы написать в этот чат", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
        elif code == 6 or code == 9:
            task_abort.main(mess, c, connection, vk, m, bot, tel, spy = 1)
            send.messages.send(message = "Ошибка создания задания (6/9), слишком много запросов в секунду, попробуйте еще раз", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
        elif code == 10:
            task_abort.main(mess, c, connection, vk, m, bot, tel, spy = 1)
            send.messages.send(message = "Ошибка создания задания (10), внутренняя ошибка Вконтакте, попробуйте еще раз", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
        elif code == 14:
            task_abort.main(mess, c, connection, vk, m, bot, tel, spy = 1)
            send.messages.send(message = "Ошибка дсоздания задания (14), невозможно выполнить проверку задания из-за капчи, попробуйте еще раз", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
        elif code == 5:
            keyboard.add_line()
            keyboard.add_button('Токен', color=VkKeyboardColor.PRIMARY)
            task_abort.main(mess, c, connection, vk, m, bot, tel, spy = 1)
            send.messages.send(message = "Ошибка создания задания (5), невозможно создать задание из-за неверного токена. Чтобы еще раз получить токен, напишите Токен", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
        else:
            task_abort.main(mess, c, connection, vk, m, bot, tel, spy = 1)
            send.messages.send(message = "Неизвестная ошибка создания задания, еще раз", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
        return "failed"
    
    import time
    if israndom == 1:
        report_delay = "случайное время в {}".format(mess)
        random_delay = random.randint(int(delay/4), int(delay*4))
        c.execute("update jobs set delay = {}, time = {}, maxdelay = 1 where id = {}".format(delay, round(time.time()) + random_delay, task_id))
    else:
        report_delay = mess
        c.execute("update jobs set delay = {}, time = {}, maxdelay = 0 where id = {}".format(delay, round(time.time()) + delay, task_id))
    c.execute("update users set status = 'menu' where id = {}".format(m["peer_id"]))
    connection.commit()
    
    send.messages.send(message = "Задание успешно создано\n\nИнформация о задании:\nID чата: {}\n{}: {}\nЧастота: {}\nСообщение: {}\n\nДля удаления задания напишите: Удалить {}".format(
        task_info[1], name_text, chat_name, report_delay, task_info[5], task_info[6]), random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], bot=bot, tel=tel)
    time.sleep(2)
    import menu
    menu.main(mess, c, connection, vk, m, bot, tel)

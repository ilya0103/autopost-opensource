def main(mess, c, connection, vk, m, bot, tel):
    c.execute("select paytime from users where id = {}".format(m["peer_id"]))
    paytime = c.fetchone()
    from vk_api.keyboard import VkKeyboard, VkKeyboardColor
    keyboard = VkKeyboard(one_time=True)
    import random
    import send
    if paytime == None:
        keyboard.add_button('Токен', color=VkKeyboardColor.PRIMARY)
        send.messages.send(message = "Вы не авторизовались в боте\nЧтобы получить инструкцию по авторизации, нажмите на кнопку ниже или напишиите Токен", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
        return "failed"
    paytime = paytime[0]
    import time
    if round(time.time()) > paytime:
        send.messages.send(message = "Премиум не активирован", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], tel=tel, bot=bot)
    else:
        time_left = paytime - round(time.time())
        if time_left < 100:
            time_text = "{} сек".format(time_left)
        elif time_left < 5000:
            time_text = "{} мин".format(round(time_left/60))
        elif time_left < 86400:
            time_text = "{} час".format(round(time_left/3600))
        else:
            time_text = "{} дн".format(round(time_left/86400))
            
        send.messages.send(message = "Премиум действует\nДо конца премиума осталось: {}".format(time_text), random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], tel=tel, bot=bot)
        
    keyboard.add_button('Купить премиум на 1 месяц', color=VkKeyboardColor.PRIMARY)
    keyboard.add_line()
    keyboard.add_button('Купить премиум на 2 месяца', color=VkKeyboardColor.PRIMARY)
    keyboard.add_line()
    keyboard.add_button('Купить премиум на 3 месяца', color=VkKeyboardColor.PRIMARY)
    keyboard.add_line()
    keyboard.add_button('Купить премиум на 6 месяцев', color=VkKeyboardColor.PRIMARY)
    keyboard.add_line()
    keyboard.add_button('Меню', color=VkKeyboardColor.PRIMARY)
    send.messages.send(message = "Возможности премиума:\n\n1. Отсутствие ограничений на количество создаваемых заданий\n2. Возможность установить частоту выполнения заданий вплоть до 30 секунд\n3. Создание заданий с рандомной задержкой\n 4. Задания на нажатия кнопок\n\nСтоимость 31р на 1 месяц, 50р на 2, 70р на 3, 130р на 6 месяцев", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), tel=tel, bot=bot)
-- -------------------------------------------------------------
-- TablePlus 5.1.2(472)
--
-- https://tableplus.com/
--
-- Database: autopost
-- Generation Time: 2023-01-01 23:47:31.1780
-- -------------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


CREATE TABLE `captcha` (
  `jobid` int(11) NOT NULL,
  `keyword` text DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  PRIMARY KEY (`jobid`),
  UNIQUE KEY `id` (`jobid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

CREATE TABLE `history` (
  `time` int(11) DEFAULT NULL,
  `users_all` int(11) DEFAULT NULL,
  `users_pay` int(11) DEFAULT NULL,
  `premium_active` int(11) DEFAULT NULL,
  `jobs_all` int(11) DEFAULT NULL,
  `jobs_enable` int(11) DEFAULT NULL,
  `groups_active` int(11) DEFAULT NULL,
  `messages` int(11) DEFAULT NULL,
  `messages_group` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

CREATE TABLE `jobs` (
  `user_id` int(11) NOT NULL,
  `to_id` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `delay` int(11) NOT NULL,
  `maxdelay` int(11) NOT NULL,
  `text` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `id` int(11) NOT NULL,
  `conn` int(11) NOT NULL,
  `fails` int(11) NOT NULL,
  `start_send` int(11) NOT NULL,
  `enabled` int(11) NOT NULL,
  `payload` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `token` text NOT NULL,
  `lastactive` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

CREATE TABLE `pays` (
  `payid` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

CREATE TABLE `premium` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

CREATE TABLE `stats` (
  `messages` int(11) NOT NULL,
  `api` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `token` text NOT NULL,
  `status` text NOT NULL,
  `paytime` int(11) NOT NULL,
  `fails` int(11) NOT NULL,
  `tid` int(11) DEFAULT NULL,
  `vkid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
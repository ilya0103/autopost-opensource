def main (mess, c, connection, vk, m, bot, tel):
    import send
    import random
    from vk_api.keyboard import VkKeyboard, VkKeyboardColor
    keyboard = VkKeyboard(one_time=True)
    keyboard.add_button('Отмена', color=VkKeyboardColor.PRIMARY)

    try:
        mess = int(mess)
    except:
        pass

    if type(mess) is int and mess <= 10:
        import redis
        import config
        r = redis.Redis(host=config.redis_ip, port=6379, db=1)
        chat_id = r.get("autopost.chatlist.{}.{}".format(m["peer_id"], mess)).decode("utf-8")

    else:
        mess = str(mess)
        mess = mess.replace("https://vk.com/", "")
        mess = mess.replace("https://m.vk.com/", "")
        mess = mess.replace("http://vk.com/", "")
        mess = mess.replace("vk.com/", "")

        import os
        login_token = os.environ.get('autopost_token')

        link = "https://api.vk.com/method/utils.resolveScreenName?&access_token={}&screen_name={}&v=5.101".format(login_token, mess)
        import requests
        info = requests.get(link, timeout=10).json()
        if "response" in info and info["response"] != []:
            if info["response"]["type"] == "group":
                chat_id = "-{}".format(info["response"]["object_id"])
            else:
                chat_id = info["response"]["object_id"]
        else:
            send.messages.send(message="Группы или профиля с этим ID/ссылкой не существует.\nЧтобы указать в какой чат отправлять сообщения, перешлите сюда ссылку на это сообщество/профиль или ID группы", random_id=random.randint(1000000000, 1000000000000), peer_id=m["peer_id"], keyboard=keyboard.get_keyboard(), tel=tel, bot=bot)
            return "err"

    c.execute("select max(id) from jobs")
    max_id = c.fetchone()[0]
    if max_id == None:
        max_id = 0

    c.execute("insert into jobs values ({}, {}, 0, 0, 0, 0, {}, 0, 0, 0, 1, 0)".format(m["peer_id"], chat_id, max_id + 1))
    connection.commit()
    c.execute("update users set status = 'task_text' where id = {}".format(m["peer_id"]))
    connection.commit()
    keyboard = VkKeyboard(one_time=True)
    import create_premium_abort
    if create_premium_abort.check(mess, c, connection, vk, m, bot, tel, position="select_button") == "failed":
        keyboard.add_button('Выбор кнопки', color=VkKeyboardColor.NEGATIVE)
    else:
        keyboard.add_button('Выбор кнопки', color=VkKeyboardColor.PRIMARY)
    keyboard.add_line()
    keyboard.add_button('Отмена', color=VkKeyboardColor.PRIMARY)
    send.messages.send(message="Введите сообщение, которое будет отправляться\nЕсли нужно нажимать на кнопку, то нажмите Выбор кнопки", random_id=random.randint(1000000000, 1000000000000), peer_id=m["peer_id"], keyboard=keyboard.get_keyboard(), tel=tel, bot=bot)

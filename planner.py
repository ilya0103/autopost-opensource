def planner(job_query, thread_fix):
    import threading
    import sender
    thread_fix2 = 0
    for job in job_query:
        send = threading.Thread(target=sender.main, args=(job, thread_fix2))
        send.start()

def main():
    while True:
        #if True:
        try:
            import pymysql
            import time
            import threading
            import config
            connection = pymysql.connect(host=config.db_ip,
                                         user=config.db_login,
                                         password=config.db_password,
                                         db=config.db_name,
                                         charset='utf8mb4')
            c = connection.cursor()
            messages = 0
            thread_fix = 0
            while True:
                time_update = round(time.time())
                c.execute("select exists(select * from jobs where time < {} and fails < 10 and delay != 0 and maxdelay = 0 and enabled = 1)".format(time_update))
                exists = c.fetchone()[0]
                if exists == 1:
                    c.execute("update jobs set start_send = {}, time = time + delay where time < {} and fails < 10 and delay != 0 and maxdelay = 0 and enabled = 1".format(time_update, time_update))
                    connection.commit()
                    c.execute("select * from jobs where start_send = {}".format(time_update))
                    job_query = c.fetchall()
                    messages = messages + len(job_query)
                    upd = threading.Thread(target=planner, args=(job_query, thread_fix))
                    upd.start()
                if messages > 50:
                    print("Обновление статистики")
                    c.execute("update stats set messages = messages + {}, api = api + {}".format(messages, messages))
                    c.execute("select max(time) from history")
                    c.execute("update history set messages = messages + {} where time = {}".format(messages, c.fetchone()[0]))
                    messages = 0
                connection.commit()
                time.sleep(1)
        #if False:
        except:
            print("planner crash")
            import time
            time.sleep(1)
            pass
main()

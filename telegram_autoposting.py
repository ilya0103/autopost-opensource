import time

while True:
    #if True:
    try:
        import config
        import requests
        import telebot
        import random
        import pymysql
        import threading
        import handler


        token = config.telegram_token
        bot = telebot.TeleBot(token)

        offset = ""
        print("Авторизация успешна")
        thread_fix = 0
        while True:
            updates = requests.get("https://api.telegram.org/bot{}/getUpdates?offset={}&timeout=600".format(token, offset)).json()

            if updates["ok"] == True:
                for update in updates["result"]:
                    if "message" in update:
                        m = update["message"]
                        t3 = threading.Thread(target=handler.main, args=(thread_fix, m, 1), daemon=True)
                        t3.start()
                    offset = update["update_id"] + 1
            else:
                offset = ""
    except:
    #if False:
        print("---------Перезапуск---------")
        time.sleep(5)

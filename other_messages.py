def main(mess, c, connection, vk, m, bot, tel, auth):
    import send
    from vk_api.keyboard import VkKeyboard, VkKeyboardColor
    keyboard = VkKeyboard(one_time=True)
    import random
    if auth == 0:
        keyboard.add_button('Авторизация', color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        keyboard.add_button('Меню', color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        keyboard.add_button('Помощь', color=VkKeyboardColor.PRIMARY)
        send.messages.send(message = "Привет! Похоже что ты не авторизовался в боте\nЧтобы пройти авторизацию набери Авторизация :(\nСписок команд можно увидеть набрав Помощь\n\nРабота с ботом происходит с помощью кнопок. Если у тебя клиент Вконтакте не поддерживает кнопки, то попробуй зайти через браузер", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), tel=tel, bot=bot)
    else:
        keyboard.add_button('Помощь', color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        keyboard.add_button('Меню', color=VkKeyboardColor.PRIMARY)
        send.messages.send(message = "Не верная команда :(\nСписок команд можно увидеть набрав Помощь\n\nРабота с ботом происходит с помощью кнопок. Если у тебя клиент Вконтакте не поддерживает кнопки, то попробуй зайти через браузер", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), tel=tel, bot=bot)

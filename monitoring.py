import time
import pymysql
import config

while True:
    #if True:
    try:
        while True:
            connection = pymysql.connect(host=config.db_ip, user=config.db_login, password=config.db_password, db=config.db_name, charset='utf8mb4')
            c = connection.cursor()

            c.execute("select count(*) from jobs")
            jobs_count = c.fetchone()[0]

            c.execute("select count(*) from jobs where enabled = 1")
            jobs_enabled = c.fetchone()[0]

            c.execute("select count(*) from users")
            users_all = c.fetchone()[0]

            c.execute("select count(*) from users where paytime > 0")
            users_played = c.fetchone()[0]

            c.execute("select count(*) from users where paytime > UNIX_TIMESTAMP()")
            premium_active = c.fetchone()[0]

            c.execute("select count(*) from login where lastactive > {}".format(round(time.time()) - 400))
            groups_active = c.fetchone()[0]

            c.execute("insert into history (time, users_all, users_pay, premium_active, jobs_all, jobs_enable, groups_active, messages, messages_group) values ({}, {}, {}, {}, {}, {}, {}, 0, 0)".format(
                round(time.time()), users_all, users_played, premium_active, jobs_count, jobs_enabled, groups_active))
            connection.commit()

            time.sleep(300)
    except:
    #if False:
        print("err")
        time.sleep(10)
        pass
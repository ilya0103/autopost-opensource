def random_planner(job, thread_fix):
    import threading
    import sender
    import pymysql
    import random
    import config
    thread_fix2 = 0
    connection = pymysql.connect(host=config.db_ip,
                                         user=config.db_login,
                                         password=config.db_password,
                                         db=config.db_name,
                                         charset='utf8mb4')
    c = connection.cursor()
    random_delay = random.randint(int(job[3] / 4), int(job[3] * 4))
    c.execute("update jobs set time = time + {} where id = {}".format(random_delay, job[6]))
    connection.commit()
    connection.close()
    send = threading.Thread(target=sender.main, args=(job, thread_fix2))
    send.start()


def main():
    while True:
        try:
            import pymysql
            import time
            import threading
            import config
            connection = pymysql.connect(host=config.db_ip,
                                                 user=config.db_login,
                                                 password=config.db_password,
                                                 db=config.db_name,
                                                 charset='utf8mb4')
            c = connection.cursor()
            messages = 0
            thread_fix = 0
            while True:
                time_update = round(time.time())
                c.execute("select * from jobs where time < {} and fails < 10 and delay != 0 and maxdelay = 1 and enabled = 1".format(time_update))
                random_jobs = c.fetchall()
                threads = []
                for job in random_jobs:
                    t = threading.Thread(target=random_planner, args=(job, thread_fix))
                    threads.append(t)
                for thread in threads:
                    thread.start()
                for thread in threads:
                    thread.join()
                    messages = messages + 1
                if messages > 50:
                    print("Обновление статистики")
                    c.execute("update stats set messages = messages + {}, api = api + {}".format(messages, messages))
                    c.execute("select max(time) from history")
                    c.execute("update history set messages = messages + {} where time = {}".format(messages, c.fetchone()[0]))
                    messages = 0
                connection.commit()
                time.sleep(1)
        except:
            print("random planner crash")
            import time
            time.sleep(1)
main()
def main(mess, c, connection, vk, m, bot, tel):
    import send
    import create_premium_abort
    if create_premium_abort.check(mess, c, connection, vk, m, bot, tel, position = "create") == "pass":
        import random
        try:
            source_id = m["fwd_messages"][0]["from_id"]
        except:
            return "failed"
        from vk_api.keyboard import VkKeyboard, VkKeyboardColor
        keyboard = VkKeyboard(one_time=True)
        keyboard.add_button('Да, все верно', color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        keyboard.add_button('Нет, отмена', color=VkKeyboardColor.PRIMARY)
        if str(source_id)[0] == "-":
            if source_id == -175400342 or source_id == -184775442:
                send.messages.send(message = "Нельзя писать автопостом в автопост :)", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], tel=tel, bot=bot)
                import menu
                menu.main(mess, c, connection, vk, m)
                return "failed"
            try:
                group_info = vk.groups.getById(group_ids = str(source_id)[1:])
            except:
                return "failed"
            send.messages.send(message = "Это сообщение от ГРУППЫ:\n\nНазвание: {}\nID: {}".format(group_info[0]["name"], source_id), random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], tel=tel, bot=bot)
            send.messages.send(message = "Сообщения будут отправляться в эту группу, все верно?", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), tel=tel, bot=bot)
        else:
            try:
                user_info = vk.users.get(user_ids = source_id)
            except:
                return "failed"
            send.messages.send(message = "Это сообщение от ПРОФИЛЯ\n\nИмя: {}\nФамилия: {}\nID: {}".format(user_info[0]["first_name"], user_info[0]["last_name"], source_id), random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], tel=tel, bot=bot)
            send.messages.send(message = "Сообщения будут отправляться в этот профиль, все верно?", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), tel=tel, bot=bot)
        c.execute("select max(id) from jobs")
        c.execute("insert into jobs values ({}, {}, 0, 0, 0, 0, {}, 0, 0, 0, 1, 0)".format(m["peer_id"], source_id, c.fetchone()[0] + 1))
        connection.commit()
    else:
        return "failed"
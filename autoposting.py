import requests
import threading
import handler
import time
import os

def longpool(g_longpool, thread_fix):
    if "updates" in g_longpool:
        if g_longpool["updates"] != []:
            for act in g_longpool["updates"]:
                if act["type"] == "message_new":
                    m = act["object"]
                    if "message" in m:
                        m = m["message"]
                    t3 = threading.Thread(target=handler.main, args=(thread_fix, m, 0))
                    t3.start()
                    
dev = 1
                    
if dev == 0:
    import status_updater
    status = threading.Thread(target=status_updater.main)
    status.start()

errors = 0

while True:
    #if True:
    try:
        thread_fix = 0
        login_token = os.environ.get('autopost_token')
        login_id = os.environ.get('autopost_id')

        link = "https://api.vk.com/method/groups.getLongPollServer?&access_token={}&group_id={}&v=5.88".format(login_token, login_id)

        get_longpool_server = requests.get(link,  timeout=10).json()

        print(get_longpool_server)
        if "error" in get_longpool_server:
            if get_longpool_server["error"]["error_code"] == 27:
                print("banned")
                time.sleep(1)
                break
            if errors > 100:
                break
            time.sleep(60)
            errors = errors + 1

        link_longpool = "{}?act=a_check&key={}&ts={}".format(get_longpool_server["response"]["server"],
                                                                     get_longpool_server["response"]["key"],
                                                                     get_longpool_server["response"]["ts"])
        g_longpool = requests.get(link_longpool, timeout=60).json()

        import config
        import pymysql
        connection = pymysql.connect(host=config.db_ip,
                                     user=config.db_login,
                                     password=config.db_password,
                                     db=config.db_name,
                                     charset='utf8mb4')
        c = connection.cursor()
        c.execute("select exists(select * from login where id = {})".format(login_id))
        if c.fetchone()[0] == 0:
            c.execute("insert into login values({}, '{}', {})".format(login_id, login_token, round(time.time())))
            connection.commit()
            print("Группа внесена в базу")


        print("Авторизация успешна")
        errors = 0
        ts = g_longpool["ts"]

        active_update_time = round(time.time())

        while True:
            link_lp_messages = "{}?act=a_check&key={}&ts={}&wait=10".format(get_longpool_server["response"]["server"],
                                                                            get_longpool_server["response"]["key"],
                                                                            ts)
            g_longpool = requests.get(link_lp_messages, timeout=60).json()
            if "ts" in g_longpool:
                if active_update_time + 250 < time.time():
                    c.execute("update login set lastactive = {} where id = {}".format(round(time.time()), login_id))
                    connection.commit()
                    active_update_time = round(time.time())
                ts = g_longpool["ts"]
                t = threading.Thread(target=longpool, args=(g_longpool, thread_fix))
                t.start()
            else:
                break

    #if False:
    except:
        print("reboot")
        time.sleep(1)

def add_keys(keyboard):
    from vk_api.keyboard import VkKeyboardColor
    keyboard.add_button('Задания', color=VkKeyboardColor.PRIMARY)
    keyboard.add_line()
    keyboard.add_button('Отключить задание', color=VkKeyboardColor.PRIMARY)
    keyboard.add_button('Удалить задание', color=VkKeyboardColor.PRIMARY)
    keyboard.add_line()
    keyboard.add_button('Меню', color=VkKeyboardColor.PRIMARY)
    return keyboard
def main(mess, c, connection, vk, m, bot, tel):
    import send
    c.execute("select paytime from users where id = {}".format(m["peer_id"]))
    paytime = c.fetchone()
    import time
    from vk_api.keyboard import VkKeyboard, VkKeyboardColor
    keyboard = VkKeyboard(one_time=True)
    import random
    if paytime == None:
        keyboard.add_button('Токен', color=VkKeyboardColor.PRIMARY)
        send.messages.send(message = "Вы не авторизовались в боте\nЧтобы получить инструкцию по авторизации, нажмите на кнопку ниже или напишите Токен", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
        return "failed"
    paytime = paytime[0]
    if mess == "включить" or mess == "включить задание" or mess == "включить задания":
        import tasks_list
        tasks_list.main(mess, c, connection, vk, m, bot, tel, delete = 0, desable = 0, enable = 1)
    elif mess == "включить все" or mess == "включить всё":
        keyboard.add_button('Включить задание', color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        keyboard.add_button('Задания', color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        keyboard.add_button('Назад в меню', color=VkKeyboardColor.PRIMARY)
        send.messages.send(message = "Нельзя включить все задания разом, во избежание капчи и блокировок", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
    else:
        task_id = int(mess.split(" ")[1])

        if paytime < round(time.time()):
            c.execute("select count(*) from jobs where user_id = {} and enabled = 1 and delay != 0".format(m["peer_id"]))
            if c.fetchone()[0] > 0:
                keyboard.add_button('Премиум', color=VkKeyboardColor.PRIMARY)
                keyboard.add_line()
                keyboard.add_button('Меню', color=VkKeyboardColor.PRIMARY)
                send.messages.send(message = "Без премиума нельзя включать более одного задания\nЧтобы получить больше информации, напишите Премиум", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
                return "failed"
            c.execute("select delay from jobs where id = {}".format(task_id))
            if c.fetchone()[0] < 300:
                keyboard.add_button('Премиум', color=VkKeyboardColor.PRIMARY)
                keyboard.add_line()
                keyboard.add_button('Меню', color=VkKeyboardColor.PRIMARY)
                send.messages.send(message="Без премиума нельзя использовать задания с частотой менее 5-ти минут\nЧтобы получить больше информации, напишите Премиум", random_id=random.randint(1000000000, 1000000000000), peer_id=m["peer_id"], keyboard=keyboard.get_keyboard(), bot=bot, tel=tel)
                return "failed"
        
        c.execute("select exists (select * from jobs where user_id = {} and id = {})".format(m["peer_id"], task_id))
        if c.fetchone()[0] == 1:
            c.execute("select delay from jobs where id = {}".format(task_id))
            delay = c.fetchone()[0]
            if delay <= 60:
                c.execute("select count(*) from jobs where user_id = {} and delay <= 60 and delay != 0 and enabled = 1".format(m["peer_id"]))
                delay_count = c.fetchall()[0][0]
                if delay_count != None and delay_count > 0:
                    keyboard = add_keys(keyboard)
                    send.messages.send(user_id=m["peer_id"], message="Заданий с частотой отправки 60 сек и менее не может быть более одного во избежание капчи\nОтключите или выключите лишние заадния", random_id = random.randint(1000000000, 1000000000000), keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
                    return "failed"

            elif delay <= 120:
                c.execute("select count(*) from jobs where user_id = {} and delay <= 120 and delay != 0 and enabled = 1".format(m["peer_id"]))
                delay_count = c.fetchall()[0][0]
                if delay_count != None and delay_count > 1:
                    keyboard = add_keys(keyboard)
                    send.messages.send(user_id=m["peer_id"], message="Заданий с частотой отправки 2 минуты и менее не может быть более двух во избежание капчи\nОтключите или выключите лишние заадния", random_id = random.randint(1000000000, 1000000000000), keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
                    return "failed"

            elif delay <= 300:
                c.execute("select count(*) from jobs where user_id = {} and delay <= 300 and delay != 0 and enabled = 1".format(m["peer_id"]))
                delay_count = c.fetchall()[0][0]
                print(delay_count)
                if delay_count != None and delay_count > 4:
                    keyboard = add_keys(keyboard)
                    send.messages.send(user_id=m["peer_id"], message="Заданий с частотой отправки 5 минут и менее не может быть более пяти во избежание капчи\nОтключите или выключите лишние заадния", random_id = random.randint(1000000000, 1000000000000), keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
                    return "failed"

            elif delay <= 600:
                c.execute("select count(*) from jobs where user_id = {} and delay <= 600 and delay != 0 and enabled = 1".format(m["peer_id"]))
                delay_count = c.fetchall()[0][0]
                if delay_count != None and delay_count > 9:
                    keyboard = add_keys(keyboard)
                    send.messages.send(user_id=m["peer_id"], message="Заданий с частотой отправки 10 минут и менее не может быть более десяти во избежание капчи\nОтключите или выключите лишние заадния", random_id = random.randint(1000000000, 1000000000000), keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
                    return "failed"

            elif delay <= 1800:
                c.execute("select count(*) from jobs where user_id = {} and delay <= 1800 and delay != 0 and enabled = 1".format(m["peer_id"]))
                delay_count = c.fetchall()[0][0]
                if delay_count != None and delay_count > 29:
                    keyboard = add_keys(keyboard)
                    send.messages.send(user_id=m["peer_id"], message="Заданий с частотой отправки 30 минут и менее не может быть более 30-ти во избежание капчи\nОтключите или выключите лишние заадния", random_id = random.randint(1000000000, 1000000000000), keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
                    return "failed"

            elif delay <= 3600:
                c.execute("select count(*) from jobs where user_id = {} and delay <= 3600 and delay != 0 and enabled = 1".format(m["peer_id"]))
                delay_count = c.fetchall()[0][0]
                if delay_count != None and delay_count > 59:
                    keyboard = add_keys(keyboard)
                    send.messages.send(user_id=m["peer_id"], message="Заданий с частотой отправки 60 минут и менее не может быть более 60-ти во избежание капчи\nОтключите или выключите лишние заадния", random_id = random.randint(1000000000, 1000000000000), keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
                    return "failed"
                
            keyboard.add_button('Список заданий', color=VkKeyboardColor.PRIMARY)
            keyboard.add_line()
            keyboard.add_button('Создать задание', color=VkKeyboardColor.PRIMARY)
            keyboard.add_line()
            keyboard.add_button('Назад в меню', color=VkKeyboardColor.PRIMARY)
            
            c.execute("update jobs set enabled = 1, fails = 0 where id = {}".format(task_id))
            connection.commit()
            send.messages.send(message = "Задание {} успешно включено".format(task_id), random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
        else:
            keyboard.add_button('Задания', color=VkKeyboardColor.PRIMARY)
            keyboard.add_line()
            keyboard.add_button('Назад в меню', color=VkKeyboardColor.PRIMARY)
            send.messages.send(message = "Задания с номером {} не существует".format(task_id), random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
            
        
def main(mess, c, connection, vk, m, bot, tel):
    import redis
    import config
    import random
    import send
    from vk_api.keyboard import VkKeyboard, VkKeyboardColor
    keyboard = VkKeyboard(one_time=True)

    token = random.randint(100000000000000000000, 900000000000000000000)

    r = redis.Redis(host=config.redis_ip, port=6379, db=2)
    r.set("autopost.tokenstats.{}".format(token),m["peer_id"], ex=3000000)

    keyboard.add_button('Меню', color=VkKeyboardColor.PRIMARY)
    send.messages.send(message="Статистика отправленных заданий за последний час находится по ссылке:\n\n"
                               "http://teleproxy.ru/stats_token={}".format(token), random_id=random.randint(1000000000, 1000000000000), peer_id=m["peer_id"], keyboard=keyboard.get_keyboard(), tel=tel, bot=bot)
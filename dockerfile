from ubuntu
ENV DEBIAN_FRONTEND noninteractive.
RUN apt-get update
RUN apt-get install -y language-pack-ru
ENV LANGUAGE ru_RU.UTF-8
ENV LANG ru_RU.UTF-8
ENV LC_ALL ru_RU.UTF-8
RUN locale-gen ru_RU.UTF-8 && dpkg-reconfigure locales
run apt install python3 -y
run apt install python3-pip -y
run pip3 install requests
run pip3 install vk_api
run pip3 install pymysql
run pip3 install redis
run pip3 install telebot
run pip3 install pytelegrambotapi
copy / /home
entrypoint python3 /home/autoposting.py

def main(mess, c, connection, vk, m, bot, tel):
    import create_premium_abort
    if create_premium_abort.check(mess, c, connection, vk, m, bot, tel, position = "create") == "pass":
        import random
        import send
        import requests
        from vk_api.keyboard import VkKeyboard, VkKeyboardColor
        keyboard = VkKeyboard(one_time=True)
        keyboard.add_button('Отмена', color=VkKeyboardColor.PRIMARY)

        c.execute("select token from users where id = {}".format(m["peer_id"]))
        user_token = c.fetchone()
        if user_token == None:
            keyboard.add_button('Токен', color=VkKeyboardColor.PRIMARY)
            send.messages.send(
                message="Вы не авторизовались в боте\nЧтобы получить инструкцию по авторизации, нажмите на кнопку ниже или напишиите Токен",
                random_id=random.randint(1000000000, 1000000000000), peer_id=m["peer_id"],
                keyboard=keyboard.get_keyboard(), tel=tel, bot=bot)
            return "failed"
        user_token = user_token[0]

        get_conversations = requests.get("https://api.vk.com/method/messages.getConversations?&access_token={}&count=100&v=5.124".format(user_token)).json()
        if "error" in get_conversations:
            code = int(get_conversations["error"]["error_code"])
            if code == 5 or code == 10:
                keyboard.add_button('Токен', color=VkKeyboardColor.PRIMARY)
                send.messages.send(message = "Ошибка создания задания (5), невозможно создать задание из-за неверного токена. Чтобы еще раз получить токен, напишите Токен", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), tel=tel, bot=bot)
                return "failed"

        count = 0
        count_chats = 0
        message = "Ваши последние активные чаты:\n\n"

        groups = ""
        chats = ""

        for conversation in get_conversations["response"]["items"]:
            count_chats += 1
            if conversation["conversation"]["peer"]["type"] == "group":
                groups = groups + str(conversation["conversation"]["peer"]["local_id"]) + ","
            elif conversation["conversation"]["peer"]["type"] == "user":
                chats = chats + str(conversation["conversation"]["peer"]["id"]) + ","
            if count_chats > 10:
                break

        import redis
        import config
        r = redis.Redis(host=config.redis_ip, port=6379, db=1)
        chat_counter = 0

        if groups != "":
            message = message + "Группы:\n"
            groups_info = requests.get("https://api.vk.com/method/groups.getById?&access_token={}&group_ids={}&v=5.124".format(user_token, groups)).json()
            for group in groups_info["response"]:
                chat_counter += 1
                r.set("autopost.chatlist.{}.{}".format(m["peer_id"], chat_counter), "-{}".format(group["id"]), ex=7200)
                message = message + str(chat_counter) + "  |  " + group["name"] + "\n"

        if chats != "":
            message = message + "\nЧаты:\n"
            chats_info = requests.get("https://api.vk.com/method/users.get?&access_token={}&user_ids={}&v=5.124".format(user_token, chats)).json()
            for chat in chats_info["response"]:
                chat_counter += 1
                r.set("autopost.chatlist.{}.{}".format(m["peer_id"], chat_counter), chat["id"], ex=7200)
                message = message + str(chat_counter) + "  |  " + chat["first_name"] + " " + chat["last_name"] + "\n"

        message = message + "\nЧтобы выбрать чат, напишите его номер из списка выше\nЕсли чата в спике нет, то напишите в него и он появится\n\n"

        if tel == 1:
            message = message + "Еще можно прислать сюда ссылку на нужную группу или профиль"
        else:
            message = message + "Еще можно переслать сюда любое сообщение от группы/пользователя или любое сообщение от него"
        
        send.messages.send(message = message, random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), tel=tel, bot=bot)
        c.execute("update users set status = 'chat_name' where id = {}".format(m["peer_id"]))
        connection.commit()
    else:
        return "failed"
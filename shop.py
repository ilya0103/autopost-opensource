from flask import Flask
from flask import request

app = Flask("autopost_shop")

@app.route('/', methods=['GET', 'POST'])
def root():
    return "no bots!!\nЭто техническая страница для того чтобы удовлетворить ботов, КОТОРЫЕ ДЕРГАЮТ ЭТОТ IP 24/7"

@app.route('/alert', methods=['GET', 'POST'])
def alert():
    
    order_id = request.form["MERCHANT_ORDER_ID"]
    
    import vk_api
    import random
    import time

    import pymysql
    import config
    conn = pymysql.connect(host=config.db_ip,
                           user=config.db_login,
                           password=config.db_password,
                           db=config.db_name,
                           charset='utf8mb4')
    c = conn.cursor()

    c.execute("select * from premium where user_id = {}".format(int(order_id[:-2])))
    group_id = c.fetchone()[1]
    try:
        c.execute("select token from login where id = {}".format(group_id))
        token = c.fetchone()[0]
    except:
        import config
        token = config.telegram_token

    vk_session = vk_api.VkApi(token=token)
    vk = vk_session.get_api()
    
    from vk_api.keyboard import VkKeyboard, VkKeyboardColor
    keyboard = VkKeyboard(one_time=True)
    keyboard.add_button('Меню', color=VkKeyboardColor.PRIMARY)
    try:
        vk.messages.send(peer_id=int(order_id[:-2]), message= "Оплата премиума получена!\nПремиум продлён на оплаченное время", random_id = random.randint(1000000000, 1000000000000), keyboard = keyboard.get_keyboard())
    except:
        pass
    #try:
        #vk.messages.send(peer_id=561220917, message= "vk.com/id{} оплатил премиум".format(order_id[:-2]), random_id = random.randint(1000000000, 1000000000000))
    #except:
        #pass

    if int(order_id[-1]) == 1:
        paytime = 2678400
    elif int(order_id[-1]) == 2:
        paytime = 5356800
    elif int(order_id[-1]) == 3:
        paytime = 8035200
    elif int(order_id[-1]) == 6:
        paytime = 16070400
    else:
        paytime = 2678400

    c.execute("select paytime from users where id = {}".format(int(order_id[:-2])))
    payedtime = c.fetchone()[0]
    
    if round(time.time()) > payedtime:
        c.execute("update users set paytime = {} where id = {}".format(round(time.time()) + paytime, int(order_id[:-2])))
    else:
        c.execute("update users set paytime = {} where id = {}".format(payedtime + paytime, int(order_id[:-2])))
    conn.commit()
    try:
        import redis
        import config
        r = redis.Redis(host=config.redis_ip, port=6379)
        r.delete("autopost.users.{}".format(int(order_id[:-2])))
    except:
        pass

    return 'YES'

@app.route('/stats_token=<token>', methods=['GET', 'POST'])
def stats(token):
    import redis
    import config
    import datetime
    r = redis.Redis(host=config.redis_ip, port=6379, db=2)
    user_id = r.get("autopost.tokenstats.{}".format(token))

    if user_id == None:
        return "Токен истек или не существует"
    else:
        user_id = user_id.decode("utf-8")

    keys = sorted(r.keys("autopost.send.{}*".format(user_id)), reverse=True)

    message = '''
    <style>
   table {
    border: 4px double #000; /* Рамка вокруг таблицы */ 
    border-collapse: separate; /* Способ отображения границы */ 
    width: 1100px;  /*Ширина таблицы */ 
    border-spacing: 30px 11px; /* Расстояние между ячейками */ 
   }
   td {
    padding: 15px; /* Поля вокруг текста */ 
    font-size: 20px;
    border: 1px solid #000000; /* Граница вокруг ячеек */ 
    vertical-align: top; /* Выравнивание по верхнему краю ячеек */
   }
  </style>
  <meta name="viewport" content="width=device-width, user-scalable=yes">
  <table>\n\n'''

    send_messages_s = r.get("autopost.stats.{}.s".format(user_id))
    send_messages_f = r.get("autopost.stats.{}.f".format(user_id))

    if send_messages_s != None:
        message = message + "<font size=5><br><b>Всего отправлено сообщений: {}</b></font><br>".format(send_messages_s.decode("utf-8"))
    if send_messages_f != None:
        message = message + "<font size=5><b>Всего ошибок: {}</b><br><br></font>".format(send_messages_f.decode("utf-8"))
    else:
        message = message + "<br>"

    message = message + "<tr><th>Ошибочные сообщения (за 24 часа):</th><th>Отправленные сообщения (за 24 часа):</th></tr>"

    message_f = ""
    message_s = ""
    for key in keys:
        key_parse = key.decode("utf-8").split(".")
        if key_parse[3] == "s":
            state = "отправлено"

            date = datetime.datetime.fromtimestamp(int(key_parse[4]) + 10800)
            month = date.month
            day = date.day

            hour = date.hour
            minute = date.minute
            second = date.second

            if month < 10:
                month = "0" + str(month)
            if day < 10:
                day = "0" + str(day)

            if hour < 10:
                hour = "0" + str(hour)
            if minute < 10:
                minute = "0" + str(minute)
            if second < 10:
                second = "0" + str(second)



            message_s = message_s + "{}.{} {}:{}:{} &ensp; | &ensp; Задание {} {} &ensp; | &ensp; Номер сообщения {}<br>".format(day, month, hour, minute, second, key_parse[5], state, key_parse[6])

    for key in keys:
        key_parse = key.decode("utf-8").split(".")
        if key_parse[3] == "f":
            state = "ошибка"

            date = datetime.datetime.fromtimestamp(int(key_parse[4]) + 10800)
            month = date.month
            day = date.day

            hour = date.hour
            minute = date.minute
            second = date.second

            if month < 10:
                month = "0" + str(month)
            if day < 10:
                day = "0" + str(day)

            if hour < 10:
                hour = "0" + str(hour)
            if minute < 10:
                minute = "0" + str(minute)
            if second < 10:
                second = "0" + str(second)

            code = int(key_parse[6])

            if code == 5 or code == 17:
                err_mess = "Неверный токен"
            elif code == 902 or code == 7 or code == 10 or code == 917:
                err_mess = "Чат не доступен"
            elif code == 14:
                err_mess = "Нужен ввод капчи"
            elif code == 6 or code == 9:
                err_mess = "Флуд контроль"
            else:
                err_mess = "Неизвстная ошибка {}".format(code)

            message_f = message_f + "{}.{} {}:{}:{} &ensp; | &ensp; Задание {} {}: {}<br>".format(day, month, hour, minute, second, key_parse[5], state, err_mess)

    if message_f == "":
        message_f = "<center>Ошибок нет</center>"
    if message_s == "":
        message_s = "Отправленных сообщений нет"
    message = message + "<tr><td><font size=3>{}</font></td><td><font size=3>{}</font></td></tr>".format(message_f, message_s)
    message = message + "</table>"

    return message


app.run(host="0.0.0.0", port = 80)
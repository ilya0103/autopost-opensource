class messages:
    def send(message, random_id, peer_id, bot, tel, keyboard=""):
        if tel == 1:
            if keyboard != "":
                from telebot import types
                keyboard_tg = types.ReplyKeyboardMarkup(one_time_keyboard=1)
                import json
                import redis
                import config
                r = redis.Redis(host=config.redis_ip, port=6379)
                keyboard = json.loads(keyboard)
                print(keyboard["buttons"])
                for line in keyboard['buttons']:
                    for key in line:
                        if "payload" in key["action"] and key["action"]["payload"] != None:
                            r.set("autopost.buttons.{}.{}".format(peer_id, str.encode(key["action"]["label"])), str(key["action"]["payload"]), ex=3600)
                    if len(line) == 1:
                        keyboard_tg.row(line[0]["action"]["label"])
                    elif len(line) == 2:
                        keyboard_tg.row(line[0]["action"]["label"], line[1]["action"]["label"])
                    elif len(line) == 3:
                        keyboard_tg.row(line[0]["action"]["label"], line[1]["action"]["label"], line[2]["action"]["label"])
                    elif len(line) == 4:
                        keyboard_tg.row(line[0]["action"]["label"], line[1]["action"]["label"], line[2]["action"]["label"], line[3]["action"]["label"])

                bot.send_message(peer_id, message, reply_markup=keyboard_tg)
            else:
                bot.send_message(peer_id, message)
        else:
            import vk_api
            import login
            import os

            login_token = os.environ.get('autopost_token')

            vk_session = vk_api.VkApi(token=login_token)
            vk = vk_session.get_api()
            vk.messages.send(message=message, random_id=random_id, peer_id=peer_id, keyboard=keyboard, tel=tel, bot=bot)
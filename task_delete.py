def main(mess, c, connection, vk, m, bot, tel):
    import send
    import random
    if mess == "удалить" or mess == "удалить задания" or mess == "удалить задание":
        import tasks_list
        tasks_list.main(mess, c, connection, vk, m, bot, tel, delete = 1, desable = 0, enable = 0)
    elif mess == "удалить все" or mess == "удалить всё":
        c.execute("delete from jobs where user_id = {}".format(m["peer_id"]))
        connection.commit()
        from vk_api.keyboard import VkKeyboard, VkKeyboardColor
        keyboard = VkKeyboard(one_time=True)
        keyboard.add_button('Создать задание', color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        keyboard.add_button('Назад в меню', color=VkKeyboardColor.PRIMARY)
        send.messages.send(message = "Все задания успешно удалены", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
    else:
        task_id = int(mess.split(" ")[1])
        c.execute("select exists (select * from jobs where user_id = {} and id = {})".format(m["peer_id"], task_id))
        from vk_api.keyboard import VkKeyboard, VkKeyboardColor
        keyboard = VkKeyboard(one_time=True)
        keyboard.add_button('Список заданий', color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        keyboard.add_button('Создать задание', color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        keyboard.add_button('Назад в меню', color=VkKeyboardColor.PRIMARY)
        if c.fetchone()[0] == 1:
            c.execute("delete from jobs where id = {}".format(task_id))
            connection.commit()
            send.messages.send(message = "Задание {} успешно удалено".format(task_id), random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
        else:
            send.messages.send(message = "Задания с номером {} не существует".format(task_id), random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)

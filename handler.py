def main(thread_fix, m, tel):
    print(m)
    import time
    start_time = time.time()

    import pymysql
    import vk_api
    import telebot
    import login
    import config
    import os

    login_token = os.environ.get('autopost_token')
    login_id = os.environ.get('autopost_id')

    connection = pymysql.connect(host=config.db_ip,
                                         user=config.db_login,
                                         password=config.db_password,
                                         db=config.db_name,
                                         charset='utf8mb4')
    c = connection.cursor()
    vk_session = vk_api.VkApi(token=login_token)
    vk = vk_session.get_api()

    bot = telebot.TeleBot(config.telegram_token, threaded=False)
    mess = m["text"].lower()

    if tel == 1:
        peer = {'peer_id': m["from"]["id"]}
        m.update(peer)
        c.execute("select id from users where tid = {}".format(m["from"]["id"]))
        main_id = c.fetchone()
        if main_id != None and main_id[0] != m["from"]["id"]:
            c.execute("update users set id = {} where tid = {}".format(m["from"]["id"], m["from"]["id"]))
            c.execute("update jobs set user_id = {} where user_id = {}".format(m["from"]["id"], main_id[0]))
    else:
        c.execute("select id from users where vkid = {}".format(m["peer_id"]))
        main_id = c.fetchone()
        if main_id != None and main_id[0] != m["peer_id"]:
            c.execute("update users set id = {} where vkid = {}".format(m["peer_id"], m["peer_id"]))
            c.execute("update jobs set user_id = {} where user_id = {}".format(m["peer_id"], main_id[0]))
    connection.commit()

    print(mess)


    
    if mess.replace("https://", "").replace("http://", "")[:35] == "api.vk.com/blank.html#access_token=":
        import token_insert
        token_insert.main(mess, c, connection, vk, m, bot, tel)

    elif "payload" in m and m["payload"] != '{"command":"start"}':
        import task_text
        task_text.main(mess, c, connection, vk, m, bot, tel, israndom=0, payload=1)
    elif mess == "создать задание":
        import create_task
        create_task.main(mess, c, connection, vk, m, bot, tel)
    elif mess == "сообщения группы/профиля":
        import chat_select
        chat_select.main(mess, c, connection, vk, m, bot, tel)
    elif mess == "беседа":
        import chat_list
        chat_list.main(mess, c, connection, vk, m, bot, tel)
    elif tel == 0 and len(m["fwd_messages"]) != 0:
        import fwd_message
        fwd_message.main(mess, c, connection, vk, m, bot, tel)
    elif mess == "да, все верно":
        import task_accept
        task_accept.main(mess, c, connection, vk, m, bot, tel)
    elif mess == "нет, отмена" or mess == "отмена":
        import task_abort
        task_abort.main(mess, c, connection, vk, m, bot, tel, spy = 0)
    elif mess == "меню" or mess == "назад в меню" or mess == "выйти в меню":
        import menu
        menu.main(mess, c, connection, vk, m, bot, tel)
    elif mess == "задания" or mess == "список заданий":
        import tasks_list
        tasks_list.main(mess, c, connection, vk, m, bot, tel, delete = 0, desable = 0, enable = 0)
    elif mess == "удалить" or mess == "удалить задания" or mess == "удалить задание" or mess == "удалить все"  or mess == "удалить всё" or (mess.split(" ")[0] == "удалить" and mess.split(" ")[1].isdigit() == True):
        import task_delete
        task_delete.main(mess, c, connection, vk, m, bot, tel)
    elif mess == "включить" or mess == "включить задание" or mess == "включить задания" or mess == "включить все" or mess == "включить всё" or (mess.split(" ")[0] == "включить" and mess.split(" ")[1].isdigit() == True):
        import task_enable
        task_enable.main(mess, c, connection, vk, m, bot, tel)
    elif mess == "отключить" or mess == "выключить" or mess == "отключить задание" or mess == "выключить задание" or mess == "отключить задания" or mess == "выключить задания" or mess == "отключить все"  or mess == "отключить всё" or mess == "выключить все" or mess == "выключить всё" or ((mess.split(" ")[0] == "отключить" or mess.split(" ")[0] == "выключить") and mess.split(" ")[1].isdigit() == True):
        import task_desable
        task_desable.main(mess, c, connection, vk, m, bot, tel)
    elif mess == "токен" or mess == "авторизация" or mess == "авторизоваться":
        import token_info
        token_info.main(mess, c, connection, vk, m, bot, tel)
    elif mess == "премиум":
        import premium
        premium.main(mess, c, connection, vk, m, bot, tel)
    elif mess == "купить премиум на 1 месяц" or mess == "купить премиум на 2 месяца" or mess == "купить премиум на 3 месяца" or mess == "купить премиум на 6 месяцев":
        import pay
        pay.main(mess, c, connection, vk, m, bot, tel)
    elif mess == "начать" or mess == "\start" or mess == "start" or mess == "/start":
        import start
        start.main(mess, c, connection, vk, m, bot, tel)
    elif mess == "помощь":
        import faq
        faq.main(mess, c, connection, vk, m, bot, tel)
    elif mess == "случайное время":
        import task_random_time
        task_random_time.main(mess, c, connection, vk, m, bot, tel)
    elif mess == "выбор кнопки" or mess == "кнопка в сообщении" or mess == "кнопка в клавиатуре":
        import button_select
        button_select.main(mess, c, connection, vk, m, bot, tel)
    elif mess == "новая группа":
        import new_group
        new_group.main(mess, c, connection, vk, m, bot, tel)
    elif mess == "статистика":
        import stats
        stats.main(mess, c, connection, vk, m, bot, tel)
    elif mess == "антикапча":
        import captcha
        captcha.list(mess, c, connection, vk, m, bot, tel, action="list")
    elif mess == "создать ключевое слово":
        import captcha
        captcha.create(mess, c, connection, vk, m, bot, tel)
    elif mess.split(" ")[0] == "удалить" and (mess.split(" ")[1] == "слово" or mess.split(" ")[1] == "ключевое"):
        import captcha
        captcha.delete(mess, c, connection, vk, m, bot, tel)
    else:
        if mess != "проверка успешна":
            c.execute("select status from users where id = {}".format(m["peer_id"]))
            user_status = c.fetchone()
            if user_status != None:
                user_status = user_status[0]
                if user_status == "chat_id":
                    import task_chat_id
                    task_chat_id.main(mess, c, connection, vk, m, bot, tel)
                elif user_status == "task_text":
                    import task_text
                    task_text.main(mess, c, connection, vk, m, bot, tel, israndom = 0, payload=0)
                elif user_status == "task_time":
                    import task_time
                    task_time.main(mess, c, connection, vk, m, bot, tel, israndom = 0)
                elif user_status == "task_random_time":
                    import task_time
                    task_time.main(mess, c, connection, vk, m, bot, tel, israndom = 1)
                elif user_status == "chat_name":
                    import chat_name
                    chat_name.main(mess, c, connection, vk, m, bot, tel)
                elif user_status == "task_button":
                    import task_text
                    task_text.main(mess, c, connection, vk, m, bot, tel, israndom=0, payload=0)
                elif user_status == "keyword_insert":
                    import captcha
                    captcha.insert(mess, c, connection, vk, m, bot, tel)
                else:
                    import other_messages
                    other_messages.main(mess, c, connection, vk, m, bot, tel, auth = 1)
            else:
                import other_messages
                other_messages.main(mess, c, connection, vk, m, bot, tel, auth = 0)


    c.execute("select exists (select * from premium where user_id = {})".format(m["peer_id"]))
    if tel == 1:
        group_id = 1
    else:
        group_id = login_id
    if c.fetchone()[0] == 0:
        c.execute("insert into premium values ({}, {})".format(m["peer_id"], group_id))
    else:
        c.execute("update premium set group_id = {} where user_id = {}".format(group_id, m["peer_id"]))
    connection.commit()

    from datetime import datetime
    print("{} {}с {} {}".format(datetime.utcfromtimestamp(m["date"]).strftime('%H:%M:%S'), round(time.time() - start_time, 2), m["peer_id"], m["text"]))

    c.execute("select max(time) from history")
    c.execute("update history set messages_group = messages_group + 1 where time = {}".format(c.fetchone()[0]))
    connection.commit()
    c.close()
    connection.close()

    return "ok"
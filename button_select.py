def main(mess, c, connection, vk, m, bot, tel):
    import create_premium_abort
    import random
    import send
    from vk_api.keyboard import VkKeyboard, VkKeyboardColor
    keyboard = VkKeyboard(one_time=True)
    if create_premium_abort.check(mess, c, connection, vk, m, bot, tel, position="select_button") == "failed":
        keyboard.add_button('Премиум', color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        keyboard.add_button('Отмена', color=VkKeyboardColor.PRIMARY)
        send.messages.send(message="Задания с нажатиями на кнопки доступны только для премиум пользователей", random_id=random.randint(1000000000, 1000000000000), peer_id=m["peer_id"], keyboard=keyboard.get_keyboard(), tel=tel, bot=bot)
        return "pass"
    c.execute("update users set status = 'task_button' where id = {}".format(m["peer_id"]))
    connection.commit()
    c.execute("select * from users where id = {}".format(m["peer_id"]))
    user_info = c.fetchone()
    if user_info[4] > 1:
        import task_abort
        keyboard.add_button('Токен', color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        keyboard.add_button('Отмена', color=VkKeyboardColor.PRIMARY)
        send.messages.send(message="Похоже что ваш токен более не действителен, обновите токен", random_id=random.randint(1000000000, 1000000000000), peer_id=m["peer_id"], keyboard=keyboard.get_keyboard(), tel=tel, bot=bot)
        task_abort.main(mess, c, connection, vk, bot, tel, m, spy=0)
        return "err"
    c.execute("select max(id) from jobs where user_id = {}".format(m["peer_id"]))
    task_id = c.fetchone()[0]
    c.execute("select * from jobs where id = {}".format(task_id))
    task_info = c.fetchone()

    import requests

    link_conversations = "https://api.vk.com/method/messages.getConversationsById?peer_ids={}&access_token={}&v=5.101".format(task_info[1], user_info[1])
    conversation_info = requests.get(link_conversations, timeout=60).json()
    link_message_info = "https://api.vk.com/method/messages.getHistory?peer_id={}&access_token={}&count=1&v=5.124".format(task_info[1], user_info[1])
    message_info = requests.get(link_message_info, timeout=60).json()
    if "error" in conversation_info:
        send.messages.send(message="Невозможно получить данные о чате. Проверьте ID чата и попробуйте авторизоваться заново", random_id=random.randint(1000000000, 1000000000000), peer_id=m["peer_id"], tel=tel, bot=bot)
        import task_abort
        task_abort.main(mess, c, connection, vk, m, bot, tel, spy=0)
        return "err"
    if mess == "выбор кнопки":
        if "current_keyboard" in conversation_info["response"]["items"][0]:
            for line in conversation_info["response"]["items"][0]["current_keyboard"]["buttons"]:
                for button in line:
                    if button["action"]["payload"] == "" or button["action"]["payload"] == "{}":
                        payload = '{"button":"ne_trigai_autoposting"}'
                    else:
                        payload = button["action"]["payload"]
                    keyboard.add_button(button["action"]["label"], color=VkKeyboardColor.PRIMARY, payload=payload)
                keyboard.add_line()
            keyboard.add_button("Отмена", color=VkKeyboardColor.PRIMARY)
            print(message_info["response"]["items"][0])
            if "keyboard" in message_info["response"]["items"][0]:
                keyboard.add_button("Кнопка в сообщении", color=VkKeyboardColor.PRIMARY)

            c.execute("select status from users where id = {}".format(m["peer_id"]))
            user_status = c.fetchone()[0]
            if user_status == "task_button":
                send.messages.send(message="Вот кнопки, которые прислал бот, выберите нужную:",
                                 random_id=random.randint(1000000000, 1000000000000), peer_id=m["peer_id"],
                                 keyboard=keyboard.get_keyboard(), tel=tel, bot=bot)
                return "ok"
        else:
            send.messages.send(message="В выбранном чате в настоящее время не вызвана клавиатура\nВызовите сообщение с клавиатурой и попробуйте еще раз", random_id=random.randint(1000000000, 1000000000000), peer_id=m["peer_id"], tel=tel, bot=bot)
            import task_abort
            task_abort.main(mess, c, connection, vk, m, bot, tel, spy=0)
    else:
        if "keyboard" in message_info["response"]["items"][0]:
            for line in message_info["response"]["items"][0]["keyboard"]["buttons"]:
                for button in line:
                    if button["action"]["payload"] == "" or button["action"]["payload"] == "{}":
                        payload = '{"button":"ne_trigai_autoposting"}'
                    else:
                        payload = button["action"]["payload"]
                    keyboard.add_button(button["action"]["label"], color=VkKeyboardColor.PRIMARY, payload=payload)
                keyboard.add_line()
            keyboard.add_button("Отмена", color=VkKeyboardColor.PRIMARY)
            if "current_keyboard" in conversation_info["response"]["items"][0]:
                keyboard.add_button("Кнопка в клавиатуре", color=VkKeyboardColor.PRIMARY)

            c.execute("select status from users where id = {}".format(m["peer_id"]))
            user_status = c.fetchone()[0]
            if user_status == "task_button":
                send.messages.send(message="Вот кнопки, которые прислал бот, выберите нужную:",
                                 random_id=random.randint(1000000000, 1000000000000), peer_id=m["peer_id"],
                                 keyboard=keyboard.get_keyboard(), tel=tel, bot=bot)
                return "ok"
        else:
            send.messages.send(message="В выбранном чате в настоящее время нет сообщения с кнопками", random_id=random.randint(1000000000, 1000000000000), peer_id=m["peer_id"], tel=tel, bot=bot)
            import task_abort
            task_abort.main(mess, c, connection, vk, m, bot, tel, spy=0)

def main(job, thread_fix2):
    import time
    start_time = time.time()
    import pymysql
    import config
    if (job[2] + job[3]) < int(time.time()):
        conn = pymysql.connect(host=config.db_ip,
                               user=config.db_login,
                               password=config.db_password,
                               db=config.db_name,
                               charset='utf8mb4')
        c = conn.cursor()
        print("Переназначение времени таска {}".format(job[6]))
        import time
        c.execute("update jobs set time = {} + delay where id = {}".format(int(time.time()), job[6]))
        conn.commit()
    import redis
    r = redis.Redis(host=config.redis_ip, port=6379)
    import json
    user_info = r.get("autopost.users.{}".format(job[0]))
    if user_info == None:
        conn = pymysql.connect(host=config.db_ip,
                               user=config.db_login,
                               password=config.db_password,
                               db=config.db_name,
                               charset='utf8mb4')
        c = conn.cursor()
        c.execute("select * from users where id = {}".format(job[0]))
        user_info = c.fetchone()
        r.set("autopost.users.{}".format(job[0]), json.dumps(user_info), ex=3600)
    else:
        user_info = json.loads(user_info.decode("utf-8"))

    token = user_info[1]

    if user_info[0] == user_info[5]:
        tel = 1
        import telebot
        bot = telebot.TeleBot(config.telegram_token, threaded=False)
    else:
        bot = 0
        tel = 0

    import requests
    import random
    if int(user_info[3]) < round(time.time()) and job[3] < 300:
        r = redis.Redis(host=config.redis_ip, port=6379)
        r.delete("autopost.users.{}".format(int(job[0])))
        conn = pymysql.connect(host=config.db_ip,
                               user=config.db_login,
                               password=config.db_password,
                               db=config.db_name,
                               charset='utf8mb4')
        c = conn.cursor()
        c.execute("update jobs set enabled = 0 where id = {}".format(job[6]))
        conn.commit()
        c.execute("select * from premium where user_id = {}".format(int(job[0])))
        group_id = c.fetchone()[1]
        if group_id != 1:
            c.execute("select token from login where id = {}".format(group_id))
            import vk_api
            vk_session = vk_api.VkApi(token=c.fetchone()[0])
            vk = vk_session.get_api()
        message = "Премиум закончился, задание {} отключено.\nЧтобы продлить премиум, набери Премиум или нажми на кнопку ниже".format(job[6])

        from vk_api.keyboard import VkKeyboard, VkKeyboardColor
        keyboard = VkKeyboard(one_time=True)
        keyboard.add_button('Премиум', color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        keyboard.add_button('Меню', color=VkKeyboardColor.PRIMARY)
        if tel == 1:
            import send
            send.messages.send(message=message, random_id=random.randint(1000000000, 1000000000000), peer_id=job[0], keyboard=keyboard.get_keyboard(), bot=bot, tel=tel)
        else:
            vk.messages.send(message=message, random_id=random.randint(1000000000, 1000000000000), peer_id=job[0], keyboard=keyboard.get_keyboard())


    send_log = requests.get("https://api.vk.com/method/messages.send?peer_id={}&message={}&payload={}&random_id={}&access_token={}&v=5.90".format(job[1], job[5], job[11], random.randint(10000000000000000000000000000000, 99999999999999999999999999999999), token)).json()
    if "error" in send_log:
        conn = pymysql.connect(host=config.db_ip,
                               user=config.db_login,
                               password=config.db_password,
                               db=config.db_name,
                               charset='utf8mb4')
        c = conn.cursor()
        r.delete("autopost.users.{}".format(job[0]))
        c.execute("select * from premium where user_id = {}".format(int(job[0])))
        group_id = c.fetchone()[1]
        if group_id != 1:
            c.execute("select token from login where id = {}".format(group_id))
            import vk_api
            vk_session = vk_api.VkApi(token=c.fetchone()[0])
            vk = vk_session.get_api()
        from vk_api.keyboard import VkKeyboard, VkKeyboardColor
        keyboard = VkKeyboard(one_time=True)
        keyboard.add_button('Токен', color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        keyboard.add_button('Меню', color=VkKeyboardColor.PRIMARY)
        code = int(send_log["error"]["error_code"])
        import send
        if code == 5 or code == 17:
            c.execute("update users set fails = fails + 1 where id = {}".format(job[0]))
            if user_info[4] > 20:
                c.execute("update jobs set enabled = 0 where user_id = {}".format(job[0]))
                conn.commit()
                message = "Токен более не действительный, отправка заданий не возможна. Задания отключены\n\nПолучите новый токен, как делали это при первой авторизации в боте\nЧтобы получить инструкцию, наберите Токен"
                if tel == 1:
                    send.messages.send(message = message, random_id=random.randint(1000000000, 1000000000000), peer_id=job[0], keyboard=keyboard.get_keyboard(), bot=bot, tel=tel)
                else:
                    vk.messages.send(message = message, random_id = random.randint(1000000000, 1000000000000), peer_id = job[0], keyboard = keyboard.get_keyboard())
        elif code == 902 or code == 7 or code == 10 or code == 917:
            c.execute("update jobs set fails = fails + 1 where id = {}".format(job[6]))
            if job[8] > 10:
                c.execute("update jobs set enabled = 0 where id = {}".format(job[6]))
                conn.commit()
                message = "Совершено более 10 неудачных попыток отправки задания под номером {}, задание отключено\n\nСкорее всего вы потеряли доступ к чату или вас добавили в черный список :(".format(job[6])
                if tel == 1:
                    send.messages.send(message=message, random_id=random.randint(1000000000, 1000000000000), peer_id=job[0], keyboard=keyboard.get_keyboard(), bot=bot, tel=tel)
                else:
                    vk.messages.send(message = message, random_id = random.randint(1000000000, 1000000000000), peer_id = job[0], keyboard = keyboard.get_keyboard())
        elif code == 14:
            if job[3] >= 600:
                c.execute("update jobs set time = time + 60 where id = {}".format(job[6]))
        elif code == 6 or code == 9:
            pass
        c.execute("update stats set messages = messages - 1")
        conn.commit()
        conn.close()
        import redis
        r = redis.Redis(host=config.redis_ip, port=6379, db=2)
        r.set("autopost.send.{}.f.{}.{}.{}".format(job[0], round(time.time()), job[6], code), 0, ex=86400)
        r.incr("autopost.stats.{}.f".format(job[0]))
        from datetime import datetime
        print("{} {}с ошибка задания {}".format(datetime.utcfromtimestamp(start_time).strftime('%H:%M:%S'), round(time.time() - start_time, 2), job[6]))
        return "failed"
    if job[8] != 0:
        conn = pymysql.connect(host=config.db_ip,
                               user=config.db_login,
                               password=config.db_password,
                               db=config.db_name,
                               charset='utf8mb4')
        c = conn.cursor()
        c.execute("update jobs set fails = 0 where id = {}".format(job[6]))
        conn.commit()

    r = redis.Redis(host=config.redis_ip, port=6379, db=2)
    r.set("autopost.send.{}.s.{}.{}.{}".format(job[0], round(time.time()), job[6], send_log["response"]), 0, ex=86400)
    r.incr("autopost.stats.{}.s".format(job[0]))


    if "conn" in locals():
        print("DB conn")
        conn.close()
    from datetime import datetime
    print("{} {}с отправлено задание {}".format(datetime.utcfromtimestamp(start_time).strftime('%H:%M:%S'), round(time.time() - start_time, 2), job[6]))

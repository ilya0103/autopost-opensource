def main(mess, c, connection, vk, m, bot, tel, delete, desable, enable):
    c.execute("select * from jobs where user_id = {} and delay != 0".format(m["peer_id"]))
    jobs_list = c.fetchall()
    import random
    import send
    from vk_api.keyboard import VkKeyboard, VkKeyboardColor
    keyboard = VkKeyboard(one_time=True)
    if len(jobs_list) == 0:
        keyboard.add_button('Создать задание', color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        keyboard.add_button('Меню', color=VkKeyboardColor.PRIMARY)
        send.messages.send(message = "У вас нет активных заданий", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), tel=tel, bot=bot)
        return "ok"
    message = "Список активных заданий:\n\nНомер | ID чата | Частота | Текст\n"
    for job in jobs_list:
        if job[10] == 0:
            enabled = "(выкл) "
        else:
            enabled = ""
        if job[4] == 1:
            israndom = "~"
        else:
            israndom = ""
        if job[1] > 2000000000:
            conv_id = job[1] - 2000000000
        else:
            conv_id = job[1]
        message = message + "{}{} | {} | {}{}с | {}\n".format(enabled, job[6], conv_id, israndom, job[3], job[5])
    if delete == 1:
        message = message + "\nЧтобы удалить задание, введите Удалить (номер задания)\nНапример: Удалить {}".format(job[6])
    elif desable == 1:
        message = message + "\nЧтобы отключить задание, введите Отключить (номер задания)\nНапример: Отключить {}".format(job[6])
    elif enable == 1:
        message = message + "\nЧтобы включить задание, введите Включить (номер задания)\nНапример: Включить {}".format(job[6])
    if delete == 0 and enable == 0 and desable == 0:
        keyboard.add_button('Создать задание', color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        keyboard.add_button('Удалить задание', color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        keyboard.add_button('Включить задание', color=VkKeyboardColor.PRIMARY)
        keyboard.add_button('Отключить задание', color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        keyboard.add_button('Статистика', color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
    keyboard.add_button('Назад в меню', color=VkKeyboardColor.PRIMARY)
    for x in range(0, len(message), 2000):
        send.messages.send(message = message[x:x + 2000], random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), tel=tel, bot=bot)
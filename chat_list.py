def main(mess, c, connection, vk, m, bot, tel):
    import create_premium_abort
    import send
    if create_premium_abort.check(mess, c, connection, vk, m, bot, tel, position = "create") == "pass":
        import random
        c.execute("select token from users where id = {}".format(m["peer_id"]))
        user_token = c.fetchone()
        from vk_api.keyboard import VkKeyboard, VkKeyboardColor
        keyboard = VkKeyboard(one_time=True)
        if user_token == None:
            keyboard.add_button('Токен', color=VkKeyboardColor.PRIMARY)
            send.messages.send(message = "Вы не авторизовались в боте\nЧтобы получить инструкцию по авторизации, нажмите на кнопку ниже или напишиите Токен", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), tel=tel, bot=bot)
            return "failed"
        user_token = user_token[0]
        keyboard.add_button('Отмена', color=VkKeyboardColor.PRIMARY)
        import requests
        get_conversations = requests.get("https://api.vk.com/method/messages.getConversations?&access_token={}&count=50&v=5.88".format(user_token)).json()
        if "error" in get_conversations:
            code = int(get_conversations["error"]["error_code"])
            if code == 5 or code == 10:
                keyboard.add_button('Токен', color=VkKeyboardColor.PRIMARY)
                send.messages.send(message = "Ошибка создания задания (5), невозможно создать задание из-за неверного токена. Чтобы еще раз получить токен, напишите Токен", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), tel=tel, bot=bot)
                return "failed"
        count = 0
        message = "Ваши последние активные беседы:\n\n"
        for conversation in get_conversations["response"]["items"]:
            if conversation["conversation"]["peer"]["type"] == "chat":
                message = message + str(conversation["conversation"]["peer"]["local_id"]) + "  |  " + conversation["conversation"]["chat_settings"]["title"][:20] + "\n"
                count = count + 1
                if count == 10:
                    break
        message = message + "\nЧтобы выбрать беседу, напишите ее номер\nЕсли беседы в спике нет, то напишите в нее и она появится"
        send.messages.send(message = message, random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), tel=tel, bot=bot)
        c.execute("update users set status = 'chat_id' where id = {}".format(m["peer_id"]))
        connection.commit()
    else:
        return "failed"

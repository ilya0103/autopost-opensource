def main(mess, c, connection, vk, m, bot, tel):
    import send
    import config
    if mess == "купить премиум на 1 месяц":
        cost = 31
        paytime = "1 месяц"
        month = 1
    elif mess == "купить премиум на 2 месяца":
        cost = 50
        paytime = "2 месяца"
        month = 2
    elif mess == "купить премиум на 3 месяца":
        cost = 70
        paytime = "3 месяца"
        month = 3
    elif mess == "купить премиум на 6 месяцев":
        cost = 130
        paytime = "6 месяцев"
        month = 6
    else:
        cost = 31
        paytime = "1 месяц"
        month = 1
    import hashlib
    import requests
    hash = hashlib.md5("2345345:{}:8jt523r58:{}m{}".format(cost, m["peer_id"], month).encode('utf-8')).hexdigest()
    link = "http://www.free-kassa.ru/merchant/cash.php?m=2345345&oa={}&o={}m{}&s={}".format(cost, m["peer_id"], month, hash)
    short_link = requests.get("https://api.vk.com/method/utils.getShortLink?v=5.111&access_token=8babc42c8babc42c8babc42cd98bdf829e8&url={}".format(link.replace("&", "%26"))).json()
    #short_link = vk.utils.getShortLink(url = link)
    qiwi_link = "https://oplata.qiwi.com/create?publicKey={}&comment={}m{}&amount={}".format(config.kiwi_public, m["peer_id"], month, cost)
    short_link_qiwi = requests.get("https://api.vk.com/method/utils.getShortLink?v=5.111&access_token=8babc42c8babc42c8babc42cd98bdf829e8&url={}".format(qiwi_link.replace("&", "%26"))).json()
    #short_link_qiwi = vk.utils.getShortLink(url=qiwi_link)
    import random
    from vk_api.keyboard import VkKeyboard, VkKeyboardColor
    keyboard = VkKeyboard(one_time=True)
    keyboard.add_button('Отмена', color=VkKeyboardColor.PRIMARY)
    c.execute("select exists (select * from premium where user_id = {})".format(m["peer_id"]))
    import os
    login_id = os.environ.get('autopost_id')
    if c.fetchone()[0] == 0:
        c.execute("insert into premium values ({}, {})".format(m["peer_id"], login_id))
    else:
        c.execute("update premium set group_id = {} where user_id = {}".format(login_id, m["peer_id"]))
    connection.commit()
    send.messages.send(peer_id=m["peer_id"], message="Для оплаты премиума на {} перейдите по ссылке ниже, выберите удобный способ оплаты и совершите оплату\n\nПри удачном платеже вы получите уведомление что премиум активирован. Обычно это происходит в течении минуты\n\nВ случае если деньги были списаны, но премиум не был активирован, обращайтесь ко мне в telegram @max11999\n\nСсылка для оплаты:\n\n".format(paytime), random_id = random.randint(1000000000, 1000000000000), keyboard = keyboard.get_keyboard(), tel=tel, bot=bot)
    send.messages.send(peer_id=m["peer_id"], message="Оплата через Qiwi или банковские карты (рекомендуется)\n" + qiwi_link, random_id=random.randint(1000000000, 1000000000000), tel=tel, bot=bot)
    send.messages.send(peer_id=m["peer_id"], message="Оплата другими способами\n" + link, random_id = random.randint(1000000000, 1000000000000), tel=tel, bot=bot)

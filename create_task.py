def main(mess, c, connection, vk, m, bot, tel):
    import send
    import create_premium_abort
    if create_premium_abort.check(mess, c, connection, vk, m, bot, tel, position = "create") == "pass":
        import random
        from vk_api.keyboard import VkKeyboard, VkKeyboardColor
        keyboard = VkKeyboard(one_time=True)
        keyboard.add_button('Сообщения группы/профиля', color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        keyboard.add_button('Беседа', color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        keyboard.add_button('Отмена', color=VkKeyboardColor.PRIMARY)

        send.messages.send(message = "Выберите, куда нужно отсылать сообщения\nНа стену бот не пишет!", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), tel=tel, bot=bot)
    else:
        return "failed"
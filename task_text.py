def main(mess, c, connection, vk, m, bot, tel, israndom, payload):
    if israndom == 0:
        c.execute("select max(id) from jobs where user_id = {}".format(m["peer_id"]))
        import redis
        import config
        r = redis.Redis(host=config.redis_ip, port=6379)
        key_payload = r.get("autopost.buttons.{}.{}".format(m["peer_id"], str.encode(m["text"])))
        if key_payload != None:
            key_payload = key_payload.decode("utf-8")
            keys_del = ""
            for key in r.keys("autopost.buttons.{}.*".format(m["peer_id"])):
                r.delete(key)
        if payload == 0 and key_payload == None:
            c.execute("update jobs set text = '{}' where id = {}".format(m["text"], c.fetchone()[0]))
        else:
            if tel == 0:
                key_payload = m["payload"]
            c.execute("update jobs set text = '{}', payload = '{}' where id = {}".format(m["text"], key_payload, c.fetchone()[0]))

        c.execute("update users set status = 'task_time' where id = {}".format(m["peer_id"]))
    else:
        c.execute("update users set status = 'task_random_time' where id = {}".format(m["peer_id"]))
    connection.commit()
    import random
    from vk_api.keyboard import VkKeyboard, VkKeyboardColor
    import send
    keyboard = VkKeyboard(one_time=True)
    
    import create_premium_abort
    if create_premium_abort.check(mess, c, connection, vk, m, bot, tel, position = "time_select") == "failed":

        keyboard.add_button('30 секунд', color=VkKeyboardColor.NEGATIVE)
        keyboard.add_button('40 секунд', color=VkKeyboardColor.NEGATIVE)
        keyboard.add_button('50 секунд', color=VkKeyboardColor.NEGATIVE)
        keyboard.add_line()
        keyboard.add_button('1 минута', color=VkKeyboardColor.NEGATIVE)
        keyboard.add_button('5 минут', color=VkKeyboardColor.PRIMARY)
        keyboard.add_button('15 минут', color=VkKeyboardColor.PRIMARY)
        keyboard.add_button('30 минут', color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        
    else:
        if israndom == 0:
            keyboard.add_button('30 секунд', color=VkKeyboardColor.PRIMARY)
            keyboard.add_button('40 секунд', color=VkKeyboardColor.PRIMARY)
            keyboard.add_button('50 секунд', color=VkKeyboardColor.PRIMARY)
            keyboard.add_button('1 минута', color=VkKeyboardColor.PRIMARY)
            keyboard.add_line()
            keyboard.add_button('5 минут', color=VkKeyboardColor.PRIMARY)
            keyboard.add_button('15 минут', color=VkKeyboardColor.PRIMARY)
            keyboard.add_button('30 минут', color=VkKeyboardColor.PRIMARY)
            keyboard.add_line()
        else:
            keyboard.add_button('1 минута', color=VkKeyboardColor.PRIMARY)
            keyboard.add_button('5 минут', color=VkKeyboardColor.PRIMARY)
            keyboard.add_button('15 минут', color=VkKeyboardColor.PRIMARY)
            keyboard.add_button('30 минут', color=VkKeyboardColor.PRIMARY)
            keyboard.add_line()

    keyboard.add_button('1 час', color=VkKeyboardColor.PRIMARY)
    keyboard.add_button('2 часа', color=VkKeyboardColor.PRIMARY)
    keyboard.add_button('5 часов', color=VkKeyboardColor.PRIMARY)
    if israndom == 0:
        keyboard.add_line()
        if create_premium_abort.check(mess, c, connection, vk, m, bot, tel, position="time_select") == "failed":
            keyboard.add_button('Случайное время', color=VkKeyboardColor.NEGATIVE)
        else:
            keyboard.add_button('Случайное время', color=VkKeyboardColor.PRIMARY)
    keyboard.add_line()
    keyboard.add_button('Отмена', color=VkKeyboardColor.PRIMARY)
    if israndom == 0:
        send.messages.send(message = "Введите время, через которое будет отправляться сообщение\n\nНапример 1м, 2ч, 30с\nИли просто выберите время ниже", random_id = random.randint(1000000000, 1000000000000), peer_id = m["peer_id"], keyboard = keyboard.get_keyboard(), bot=bot, tel=tel)
    else:
        send.messages.send(message="Введите случайное время, через которое будет отправляться сообщение\n\nНапример 1м, 30м, 2ч\nИли просто выберите время ниже",
            random_id=random.randint(1000000000, 1000000000000), peer_id=m["peer_id"], keyboard=keyboard.get_keyboard(), bot=bot, tel=tel)
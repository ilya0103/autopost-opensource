def main(mess, c, connection, vk, m, bot, tel):
    import random
    import send
    from vk_api.keyboard import VkKeyboard, VkKeyboardColor
    keyboard = VkKeyboard(one_time=True)
    send.messages.send(message="При выборе случайного времени, задания будут выполняться со случайной задержкой\n"
                             "Задержка выставляется от четверти выбранного времени, до x4 от времени\n"
                             "Например если выбрать задержку в 1 минуту, то задания будут выполняться с частотой от 15 секунд, до 4 минут",
        random_id=random.randint(1000000000, 1000000000000), peer_id=m["peer_id"], bot=bot, tel=tel)
    import create_premium_abort
    if create_premium_abort.check(mess, c, connection, vk, m, bot, tel, position="time_select") == "failed":
        c.execute("update users set status = 'menu' where id = {}".format(m["peer_id"]))
        connection.commit()
        keyboard.add_button('Премиум', color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        keyboard.add_button('Отмена', color=VkKeyboardColor.PRIMARY)
        send.messages.send(message="Создание заданий со случайной задержкой доступно только для тех, у кого есть премиум", random_id=random.randint(1000000000, 1000000000000),
                         peer_id=m["peer_id"], keyboard=keyboard.get_keyboard(), bot=bot, tel=tel)
    else:
        import task_text
        task_text.main(mess, c, connection, vk, m, israndom=1, payload = 0, bot=bot, tel=tel)
